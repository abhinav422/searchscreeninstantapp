package com.example.searchscreen

import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(JobDetailsComponentModule::class))
interface JobDetailsComponent{

    fun inject(jobDetailsViewModel: JobDetailsViewModel)
}
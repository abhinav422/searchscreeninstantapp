package com.example.searchscreen

import android.arch.lifecycle.MutableLiveData
import android.os.AsyncTask
import android.util.Log
import okhttp3.*
import org.json.JSONObject
import java.net.URLEncoder
import javax.inject.Inject


class ApiRequest @Inject constructor(var searchType: SearchType, var okHttpAndServerProvider: OkHttpAndServerProvider, var client: OkHttpClient) {

    var path: String = ""
    var additionalPath: String = ""
    var mutableLiveData: MutableLiveData<JSONObject> = MutableLiveData()

    @Synchronized
    fun startRequest(listOfParams: ArrayList<Params>?) {
        if (searchType == SearchType.JOB_SEARCH) {
            path = okHttpAndServerProvider.serverUrl + "search/job"
        } else if (searchType == SearchType.JOB_DETAIL) {
            path = okHttpAndServerProvider.serverUrl + "job/profile"
        }
        path += additionalPath
        object : AsyncTask<Void, Void, String>() {
            override fun doInBackground(vararg p0: Void?): String? {
                val request = Request.Builder()
                buildHeaderParams(request)
                var url: String = buildURL(listOfParams, path)
                request.url(url)
                Log.d("jsonBodyUrl", url)
                var response: Response = client.newCall(request.build()).execute()
                return response.body()?.string()
            }

            override fun onPostExecute(result: String?) {
                Log.d("jsonBodyResponse", result)
                mutableLiveData.postValue(JSONObject(result))
            }
        }.execute()
    }

    private fun buildURL(mUrlParams: ArrayList<Params>?, path: String): String {
        var path = path
        if (mUrlParams != null) {
            var paramStr = mUrlParams[0].tag + "=" + URLEncoder.encode(mUrlParams[0].value, "utf-8")
            for (i in 1 until mUrlParams.size) {
                val param = mUrlParams.get(i)
                paramStr += "&" + param.tag + "=" + URLEncoder.encode(param.value, "utf-8")
            }
            path += if (paramStr.indexOf("?") > -1) {
                "&$paramStr"
            } else {
                "?$paramStr"
            }
        }
        return path
    }


    private fun buildHeaderParams(request: Request.Builder) {
        request.header(SearchRequestParameters.PARAM_API_KEY_HEADER, SearchRequestParameters.mApiKey)
        request.header(SearchRequestParameters.PARAM_APP_VERSION_HEADER, SearchRequestParameters.mAppVersion)
        request.header(SearchRequestParameters.PARAM_APP_BUILD_NUMBER_HEADER, SearchRequestParameters.mAppBuildNo)
        request.header(SearchRequestParameters.PARAM_OS_HEADER, SearchRequestParameters.mOS)
        request.header(SearchRequestParameters.PARAM_OS_VERSION_HEADER, SearchRequestParameters.mOSVersion)
        request.header(SearchRequestParameters.PARAM_AUTH_TOKEN_HEADER, SearchRequestParameters.mAuthToken)
    }

}
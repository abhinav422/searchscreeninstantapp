package com.example.searchscreen

import android.app.Application

class SearchScreenApplication: Application() {

    lateinit var appComponent: AppComponent
    companion object {
        lateinit var searchScreenApplication: SearchScreenApplication
        fun getInstance():SearchScreenApplication = searchScreenApplication
    }

    override fun onCreate() {
        super.onCreate()
        searchScreenApplication=this
        appComponent = DaggerAppComponent.builder().appComponentModule(AppComponentModule()).build()
    }
}

package com.example.searchscreen

import android.arch.core.util.Function
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.Transformations
import android.text.TextUtils
import android.util.Log
import com.google.gson.Gson
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import javax.inject.Inject
import com.example.searchscreen.R

class JobDetailsSearch @Inject constructor(var apiRequest: ApiRequest) {

    fun startRequest(jobId: Long) {
        apiRequest.additionalPath = "/" + jobId.toString()
        apiRequest.startRequest(null)
    }

    lateinit var jobsDetailLiveData: LiveData<JobDetails>

    init {

        jobsDetailLiveData = Transformations.switchMap(apiRequest.mutableLiveData) { input ->
            var gson = Gson()
            var jobDetailsLiveData: MutableLiveData<JobDetails> = MutableLiveData()
            var jobDetails = gson.fromJson(input?.getJSONObject("data")?.getJSONObject("general").toString(), JobDetails::class.java)
            Log.e("input", input?.getJSONObject("data")?.getJSONObject("general").toString() + "")
            Log.e("jobDetails", jobDetails.toString())
            jobDetails.jobStartDate2 = toDate(jobDetails.jobStartDate, false)
            jobDetails.jobEndDate2 = toDate(jobDetails.jobEndDate, false)
            jobDetails.appliedOn2 = toDate(jobDetails.appliedOn, false)
            jobDetails.postJobUntil2 = toDate(jobDetails.postJobUntil, false)
            jobDetails.jobPostDate2 = toDate(jobDetails.jobPostDate, false)
            if (jobDetails.jobType == "R") {
                jobDetails.availability = getAvailability(input?.getJSONObject("data")?.getJSONObject("schedule")?.getJSONArray("jobTime"))
            }
            jobDetailsLiveData.value = jobDetails
            jobDetails.imageUrl = input?.getJSONObject("data")?.getJSONObject("postedBy")?.optString("imageUrl")
            jobDetails.name = input?.getJSONObject("data")?.getJSONObject("postedBy")?.optString("name")
            jobDetails.memberType = input?.getJSONObject("data")?.getJSONObject("postedBy")?.optString("memberType")
            jobDetails.memberSubscriptionType = input?.getJSONObject("data")?.getJSONObject("postedBy")?.optString("memberSubscriptionType")
            var attributeSectionsArray = JSONArray()
            attributeSectionsArray = getAttributeSectionsJSONArray(input?.getJSONObject("data")!!, attributeSectionsArray)
            jobDetails.listOfAttributes = getAttributeSectionList(attributeSectionsArray)
            jobDetailsLiveData
        }
    }


    @Throws(ParseException::class)
    fun toDate(dateStr: String?, setTimeZone: Boolean): Date? {
        val df = SimpleDateFormat("yyyy-MM-dd'T'HH:mm")
        var date: Date? = null

        if (dateStr != null) {
            if (dateStr.isNotEmpty()) {
                if (setTimeZone) {
                    df.timeZone = TimeZone.getTimeZone("EST5EDT")
                }
                date = df.parse(dateStr)
            }
        }
        return date
    }

    @Throws(JSONException::class)
    private fun getAttributeSectionsJSONArray(data: JSONObject?, attributeSectionsArray: JSONArray): JSONArray {
        lateinit var preferencesArray: JSONArray
        // General
        val general = data?.getJSONObject("general")
        val mJobCategory = general?.getString("jobCategory")
        val serviceId = general?.getString("serviceId")

        if (mJobCategory.equals("COMPANY", ignoreCase = true)) {
            val preferencesObject = data?.getJSONObject("preferences")
            val x = preferencesObject?.keys()
            preferencesArray = JSONArray()

            while (x!=null && x.hasNext()) {
                var key = x.next() as String
                if (!(key.equals("Age Groups", ignoreCase = true) && serviceId.equals("CHILDCARE", ignoreCase = true))) {
                    val jsonObject = JSONObject()
                    jsonObject.put("id", "")
                    jsonObject.put("type", "list")
                    jsonObject.put("multiselect", false)
                    val value = JSONArray()
                    val keyValues = preferencesObject.get(key).toString()
                    val values = keyValues.split(",".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                    for (name in values) {
                        val valueJsonObject = JSONObject()
                        valueJsonObject.put("id", "")
                        valueJsonObject.put("label", name.trim { it <= ' ' })
                        valueJsonObject.put("value", true)
                        value.put(valueJsonObject)
                    }
                    jsonObject.put("value", value)
                    key = setRecruitmentJobsPreferenceTitle(key, serviceId)
                    jsonObject.put("label", key)
                    preferencesArray.put(jsonObject)
                }
            }
        } else {
            if(data!=null) {
                preferencesArray = data.getJSONArray("preferences")
            }
        }
        var attributeArray: JSONArray? = null
        var attributeSectionObject: JSONObject? = null

        // attributes sections array construction
        for (i in 0 until preferencesArray.length()) {
            val oldJSONObject = preferencesArray.get(i) as JSONObject
            var attributeObject: JSONObject? = null

            // Check if attributes need to be grouped into a section
            if (isAttributeSectionGroupingRequired(oldJSONObject)) {
                continue
            }

            // Construct attribute section JSON object
            attributeSectionObject = JSONObject()

            // title construction
            val title = oldJSONObject.optString("title")
            attributeSectionObject.put("title", title)

            attributeArray = JSONArray()

            // attributeObject = getNewAttributeJSONObject(oldJSONObject);
            attributeObject = oldJSONObject

            attributeArray.put(attributeObject)

            // Set attribute section title
            setNewAttributeSectionTitle(oldJSONObject, attributeObject, attributeSectionObject, title)

            // attributes array inclusion
            attributeSectionObject.put("attributes", attributeArray)

            // add attribute section object to its array
            attributeSectionsArray.put(attributeSectionObject)
        }

        // Construct attribute section JSON object
        attributeSectionObject = JSONObject()
        attributeArray = JSONArray()
        var grouped = false

        // Run through the attributes and group the attributes if needed
        for (i in 0 until preferencesArray.length()) {
            val oldJSONObject = preferencesArray.get(i) as JSONObject
            var attributeObject: JSONObject? = null

            if (isAttributeSectionGroupingRequired(oldJSONObject)) {
                attributeObject = oldJSONObject

                attributeArray.put(attributeObject)

                grouped = true
            }
        }

        // Attributes are grouped
        if (grouped) {
            attributeSectionObject.put("title", "CAREGIVER PREFERENCES")

            // attributes array inclusion
            attributeSectionObject.put("attributes", attributeArray)

            // add attribute section object to its array
            attributeSectionsArray.put(attributeSectionsArray.length(), attributeSectionObject)
        }
        return attributeSectionsArray

    }

    @Throws(JSONException::class)
    private fun isAttributeSectionGroupingRequired(oldJSONObject: JSONObject): Boolean {
        val id = oldJSONObject.getString("id")

        // Separate attribute sections are not required for these ids
        return id == "attr-job.comfortableWithPets" || id == "attr-job.nonSmoker" ||
                id == "ownTransportation"
    }

    @Throws(JSONException::class)
     fun setNewAttributeSectionTitle(oldJSONObject: JSONObject, attributeObject: JSONObject,
                                            attributeSectionObject: JSONObject, title: String) {
        val id = oldJSONObject.getString("id")

        if (id == "ccAgeGroups-CHLDAGEGP001" || id == "ccAgeGroups-CHLDAGEGP002" ||
                id == "ccAgeGroups-CHLDAGEGP003" || id == "ccAgeGroups-CHLDAGEGP004" ||
                id == "ccAgeGroups-CHLDAGEGP005") {
            if (id == "ccAgeGroups-CHLDAGEGP001") {
                attributeSectionObject.put("title", ("NUMBER OF CHILDREN"))
            } else {
                attributeSectionObject.put("title", "")
            }
        } else if (id == "Additional Services Needed") {
            attributeSectionObject.put("title", (R.string.additional_services_needed))
        } else {
            if (title == "") {
                attributeSectionObject.put("title", attributeObject.getString("label"))
            }
        }
    }

     fun setRecruitmentJobsPreferenceTitle(key: String, serviceId: String?): String {
        var label = key
        if (serviceId.equals("SPCLNEEDS", ignoreCase = true) && key.equals("Age Groups", ignoreCase = true)) {
            label = "Age of Care Recipients"
            return label
        } else if (serviceId.equals("HOUSEKEEP", ignoreCase = true) && key.equals("Other Services", ignoreCase = true)) {
            label = "Preferences"
            return label
        }
        when (key) {
            "Pets" -> label = "Services Needed For"
            "Specialized In" -> label = "Diagnosis"
            "Services", "Other Services" -> label = "Additional Services"
            "Subjects" -> label = "Subject Areas"
            else -> {
            }
        }
        return label
    }

    @Throws(JSONException::class)
     fun getAttributeSectionList(jsonArray: JSONArray?): List<AttributeSection>? {
        var attributeSectionList: MutableList<AttributeSection>? = null

        if (jsonArray != null) {
            attributeSectionList = ArrayList()

            for (i in 0 until jsonArray.length()) {
                val attributeSection = AttributeSection(jsonArray.get(i) as JSONObject)

                // Server needs to send "Additional Services Needed" title value. Since server is not sending, we are adding it here.
                if (TextUtils.isEmpty(attributeSection.mTitle) && attributeSection.mAttributes?.get(0)?.getId().equals("Additional Services Needed")) {
                    attributeSection.mTitle = attributeSection.mAttributes?.get(0)?.getLabel()!!
                }

                attributeSectionList.add(attributeSection)
            }
        }
        return attributeSectionList
    }

    @Throws(JSONException::class)
    fun getAvailability(availability: JSONArray?): Array<BooleanArray> {
        // Availability values exist, populate them
        if (availability != null) {
            val result = Array(availability.length()) { BooleanArray(7) }

            for (i in 0 until availability.length()) {
                val jArray = availability.getJSONArray(i)

                for (j in 0 until jArray.length()) {
                    result[i][j] = jArray.optBoolean(j)
                }
            }
            return result
        }

        // Fix for Issue # 1560
        // https://www.crashlytics.com/care/android/apps/com.care.android.careview/issues/538f4a8fe3de5099ba4bf03d/sessions/538fcbdb01f500013c683934a841661b
        // Don't return null here.
        // Even if server does not send availability time field, we need to
        // return an array with empty availability values
        return Array(7) { BooleanArray(7) }
    }

}
package com.example.searchscreen

import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable

open class Attribute() : Serializable {

    constructor(jsonObject: JSONObject) : this() {
        val type: String = jsonObject.optString("type");

        mId = jsonObject.getString("id");
        mLabel = jsonObject.getString("label");
        mMultiSelect = true

        // Map the type.
        if ((type.isEmpty()) || type.equals("boolean")) {
            mType = Type.BOOLEAN;
        } else if (type == "integer") {
            var constraints: JSONObject? = jsonObject.optJSONObject("constraints")
            if (constraints != null) {
                mMultiSelect = jsonObject.optBoolean("multiselect", false)
                mType = Type.INTEGER_GROUP;
            } else {
                mType = Type.INTEGER;
            }
        } else if (type.equals("string")) {
            mType = Type.STRING;
        } else if (type.equals("list")) {
            mMultiSelect = jsonObject.optBoolean("multiselect", false);
            mType = Type.BOOLEAN_GROUP;
        } else if (type.equals("range-integer")) {
            mType = Type.RANGE_INTEGER;
        } else if (type.equals("moreAttributes")) {
            mType = Type.MORE_ATTRIBUTES_GROUP;
            mMultiSelect = jsonObject.optBoolean("multiselect");
        } else if (type.equals("dropdown")) {
            mType = Type.BOOLEAN_GROUP;
            mMultiSelect = false;
        } else {
            throw  RuntimeException("Unknown type");
        }
        var presentationObj: JSONObject? = jsonObject.optJSONObject("presentation");
        mPresentation = if (presentationObj != null) {
            Presentation(presentationObj);
        } else {
            Presentation(JSONObject("{\n" +
                    "                    \"layer\": \"inline\"\n" +
                    "                  }"))
        }

        // List is handled in ServerAttributeBooleanGroup.
        if ((mType != Type.BOOLEAN_GROUP) && (mType != Type.MORE_ATTRIBUTES_GROUP)) {
            mValue = jsonObject.getString("value");
        }
    }

    constructor(attribute: Attribute) : this() {
        mId = attribute.getId()
        mLabel = attribute.getLabel()
        mType = attribute.getType()
        mValue = attribute.getValue()
    }

    constructor(id: String, label: String, type: Type, pres: Presentation, value: String) : this() {
        mId = id
        mLabel = label
        mType = type
        mPresentation = pres
        mValue = value
    }

    /**
     * Member Variables
     */
    private var mId: String? = null
    private var mLabel: String? = null
    private var mType: Type? = null
    private var mPresentation: Presentation? = null
    private var mMultiSelect: Boolean = false
    var mValue: String? = null

    companion object {
        @Throws(JSONException::class)
        fun serverAttributeFromJson(jsonObject: JSONObject): Attribute {
            val type = jsonObject.getString("type")

            when {
                type == "list" -> return AttributeBooleanGroup(jsonObject)
                type == "moreAttributes" -> return MoreAttributesGroup(jsonObject)
                type.contains("integer") -> {
                    val constraints = jsonObject.optJSONObject("constraints")

                    return if (constraints != null) {
                        AttributeIntegerGroup(jsonObject)
                    } else {
                        Attribute(jsonObject)
                    }
                }
                else -> return if (type == "dropdown") {
                    AttributeBooleanGroup(jsonObject)
                } else {
                    Attribute(jsonObject)
                }
            }
        }

    }


    fun getValue(): String? {
        if (mType == Type.BOOLEAN_GROUP) {
            throw  RuntimeException("Wrong type");
        }

        return mValue;
    }

    fun getId(): String? {
        return mId
    }


    /**
     * Get the label.
     */
    fun getLabel(): String? {
        return mLabel
    }


    /**
     * Get the type.
     */
    fun getType(): Type? {
        return mType
    }

    /**
     * Set the label.
     */
    fun setLabel(label: String) {
        mLabel = label
    }

    /**
     * Get the presentation.
     */
    fun getMultiSelect(): Boolean {
        return mMultiSelect
    }

    /**
     * Get the presentation.
     */
    fun getPresentation(): Presentation? {
        return mPresentation
    }


    fun toBoolean(): Boolean {
        if (mType != Type.BOOLEAN) {
            throw RuntimeException("Wrong type")
        }

        return java.lang.Boolean.parseBoolean(mValue)
    }

    fun toInt(): Int {
        if (mType != Type.INTEGER) {
            throw RuntimeException("Wrong type")
        }

        return Integer.parseInt(mValue)
    }
}


/**
 * Type
 */
enum class Type {
    BOOLEAN,
    INTEGER,
    INTEGER_GROUP,
    STRING,
    BOOLEAN_GROUP,
    RANGE_INTEGER,
    MORE_ATTRIBUTES_GROUP
}

class Presentation(json: JSONObject) : Serializable {
    private val layer: String = json.optString("layer")
    val showLabel: Boolean = json.optBoolean("showLabel")

    val isLayer: Boolean
        get() = layer == "overlay"

    companion object {
        const val serialVersionUID = 2L
    }
}
package com.example.searchscreen

class SearchRequestParameters{

    companion object{
        const val PARAM_API_KEY_HEADER = "X-Care.com-APIKey"
        const val PARAM_AUTH_TOKEN_HEADER = "X-Care.com-AuthToken"
        const val PARAM_APP_VERSION_HEADER = "X-Care.com-AppVersion"
        const val PARAM_OS_HEADER = "X-Care.com-OS"
        const val PARAM_OS_VERSION_HEADER = "X-Care.com-OSVersion"
        const val PARAM_APP_BUILD_NUMBER_HEADER = "X-Care.com-AppBuildNr"
        const val mApiKey ="TfbS5OSlsvW3XoaSFrI3CZ5JKSJQDSJnBTOZbviwdr0x"
        const val mAuthToken ="UqHtt0CcB*yLE32Hj5fnDo3Ffg6LeD9FCs33TH8CYMM."
        const val mAppVersion="9.5.1"
        const val mOS="android"
        const val mOSVersion = "21"
        const val mAppBuildNo="1066"
    }


}
package com.example.searchscreen

import org.json.JSONException
import org.json.JSONObject
import java.util.*

class AttributeIntegerGroup @Throws(JSONException::class)
internal constructor(jsonObject: JSONObject) : Attribute(jsonObject) {

    /**
     * return section header label
     */
    private val sectionHeaderLabel: String
    var interval = 0
    var min = 0
    var max = 0
    private var mMore = false
    private var mValues: MutableList<Int>? = null

    /**
     * For Range Integer
     */
    var selectedDefMin = 0
        private set
    var selectedDefMax = 0
        private set

    init {
        val type = getType()
        if (type != Type.INTEGER_GROUP && type != Type.RANGE_INTEGER) {
            throw RuntimeException("Wrong type")
        }

        sectionHeaderLabel = jsonObject.optString("labelForSectionHeader")

        //Set def values
        if (type == Type.RANGE_INTEGER) {
            val value = jsonObject.optJSONObject("value")
            if (value != null) {
                selectedDefMin = value.getInt("lowerValue")
                selectedDefMax = value.getInt("upperValue")
            }
        } else
        //i.e, Type.INTEGER_GROUP
        {
            selectedDefMax = jsonObject.optInt("value")
        }

        val constraints = jsonObject.getJSONObject("constraints")
        interval = constraints.getInt("interval")
        min = constraints.getInt("min")
        max = constraints.getInt("max")
        mMore = constraints.getBoolean("orMore")

        var i = min
        while (i <= max) {
            if (mValues == null) {
                mValues = ArrayList()
            }
            mValues!!.add(i)
            i += interval
        }
    }

    companion object {
        internal const val serialVersionUID = 1L
    }
}

package com.example.searchscreen

import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response

class OkHttpAndServerProvider{
    private var okHttpClient: OkHttpClient
    var serverUrl:String
    init {
        okHttpClient = OkHttpClient.Builder().addInterceptor { chain ->
            val request = chain?.request()
            val authenticatedRequest = request?.newBuilder()
                    ?.header("Authorization", Credentials.basic("remote","remoteUserP!"))?.build()
            chain?.proceed(authenticatedRequest)
        }.build()

        serverUrl = "https://www.uat1.carezen.net/platform/spi/"
    }

}
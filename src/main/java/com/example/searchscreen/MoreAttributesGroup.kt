package com.example.searchscreen

import org.json.JSONException
import org.json.JSONObject
import java.util.ArrayList

class MoreAttributesGroup
@Throws(JSONException::class)
internal constructor(jsonObject: JSONObject) : Attribute(jsonObject) {

    /**
     * Data Members
     */
    private val mGroupValues: ArrayList<Attribute>

    /**
     * Get the group values
     */
    val groupValues: List<Attribute>
        get() = mGroupValues

    init {
        val values = jsonObject.getJSONArray("value")

        mGroupValues = ArrayList(values.length())

        for (i in 0 until values.length()) {
            val value = values.getJSONObject(i)
            val attribute = Attribute.serverAttributeFromJson(value)

            mGroupValues.add(attribute)
        }
    }

    companion object {
        internal const val serialVersionUID = 1L
    }
}

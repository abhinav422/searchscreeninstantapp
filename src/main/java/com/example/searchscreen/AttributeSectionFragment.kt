package com.example.searchscreen

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.res.Resources
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.text.InputType
import android.text.TextUtils
import android.util.Log
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.care.patternlib.CareCheckBox
import com.care.patternlib.CareDialog
import com.care.patternlib.CustomTextView
import com.care.patternlib.NavigationItem
import java.lang.ref.WeakReference
import java.util.ArrayList

///
// A Fragment used to display and edit a section of attribute values. The title is optional.
///
class AttributeSectionFragment : Fragment {

    ///
    // Private Members
    ///
    private var mAttributeSection: AttributeSection? = null
    private var mIsEditing: Boolean = false
    private var mShowBooleanTitle: Boolean = false
    private var mShowIntegerTitle: Boolean = false
    private var mbIsUsedInJobFlow: Boolean = false
    private var mbIsUsedInJobDetailsFlow: Boolean = false
    private  var mRecJobRequirements: String? = ""
    private var mSectionLayout: LinearLayout? = null
//    private var mUpdatedAttributes: UpdatedAttributes? = null
//    private var mOverlayDialog: MultiSelectDialogUtil? = null
    private var mAttributeViewList: ArrayList<AttributeView>? = null

    ///
    //Get updated attributes for the Attribute section.
    ///
//    val updatedAttributes: UpdatedAttributes
//        get() {
//            addCurrentAttributeValues()
//            mUpdatedAttributes!!.print()
//            return mUpdatedAttributes
//        }


    ///
    // View/Control Type
    ///
    enum class ViewType {
        CHECK_BOX,
        GRID_VIEW,
        RANGE_SEEK_BAR,
        VIEWS_ATTRIBUTE_STRING,
        SINGLE_SELECT_DIALOG,
        MULTI_SELECT_DIALOG
    }

    private inner class AttributeView internal constructor(var mView: Any //can store view and Dialog
                                                           , var mViewType: ViewType, var mAttribute: Attribute)

    ///
    // Constructor
    ///
    constructor() {
        // Needed in order to restore after being purged from memory.
    }


    @SuppressLint("ValidFragment")
///
    // Constructor
    ///
    constructor(attributeSection: AttributeSection, editable: Boolean,
                isFromPAJ: Boolean, isFromJobDetails: Boolean,
                recJobRequirements: String?) {
        mAttributeSection = attributeSection
        mIsEditing = editable
        mbIsUsedInJobFlow = isFromPAJ
        mbIsUsedInJobDetailsFlow = isFromJobDetails
        mShowBooleanTitle = false
        mRecJobRequirements = recJobRequirements

        // Read only mode
        if (!mIsEditing && mAttributeSection != null) {
            for (attribute in mAttributeSection!!.mAttributes!!) {
                // Boolean type and one of the attributes is true
                if (attribute.getType() == Type.BOOLEAN && attribute.toBoolean()) {
                    mShowBooleanTitle = true
                    break
                }

                // Boolean type and one of the attributes is true
                if (attribute.getType() == Type.INTEGER && attribute.toInt() > 0) {
                    mShowIntegerTitle = true
                    break
                }
            }
        }
    }


    ///
    // On create and restore the fragment's state.
    ///
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Restore the state.
        if (savedInstanceState != null) {
            mAttributeSection = savedInstanceState.getSerializable(ATTRIBUTE_SECTION) as AttributeSection
//            mUpdatedAttributes = savedInstanceState.getSerializable(UPDATED_ATTRIBUTE) as UpdatedAttributes
            mIsEditing = savedInstanceState.getBoolean(IS_EDITING)
            mbIsUsedInJobFlow = savedInstanceState.getBoolean(IS_JOB_ACTIVITY)
            mbIsUsedInJobDetailsFlow = savedInstanceState.getBoolean(IS_JOB_DETAILS_ACTIVITY)
        } else {
//            mUpdatedAttributes = UpdatedAttributes()
        }

        mAttributeViewList = ArrayList()
    }


    ///
    // Create the dynamic layout.
    ///
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        super.onCreateView(inflater, container, savedInstanceState)
        //TODO: Remove when cleaning up after these flows are PatternLibbed
        // Inflate the layout for this fragment
        var rootView: View? = null
        //TODO is to be removed when all the related screens are moved to patternLib

        rootView = inflater!!.inflate(R.layout.attribute_section_fragment_new, container, false)
        mSectionLayout = rootView!!.findViewById(R.id.section_layout) as LinearLayout

        //Set title of AttributeSection
        val sectionTitle = rootView.findViewById(R.id.title) as TextView
        var headerView: View? = null

        if (mAttributeSection!!.mTitle.length == 0) {
            if (!(mbIsUsedInJobFlow || mbIsUsedInJobDetailsFlow)) {
                headerView!!.visibility = View.GONE
            }
            sectionTitle.visibility = View.GONE
        } else {
            if (!(mbIsUsedInJobFlow || mbIsUsedInJobDetailsFlow)) {
                headerView!!.visibility = View.VISIBLE
            }
            sectionTitle.text = mAttributeSection!!.mTitle.toUpperCase()
        }

        inflateAttributeSection(rootView)

        return rootView
    }


    ///
    // Save the fragment's state.
    ///
    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        // Store the state.
        outState!!.putSerializable(ATTRIBUTE_SECTION, mAttributeSection)
//        outState.putSerializable(UPDATED_ATTRIBUTE, mUpdatedAttributes)
        outState.putBoolean(IS_EDITING, mIsEditing)
        outState.putBoolean(IS_JOB_ACTIVITY, mbIsUsedInJobFlow)
        outState.putBoolean(IS_JOB_DETAILS_ACTIVITY, mbIsUsedInJobDetailsFlow)

        // TODO: The current values also need to be saved and restored. As it is
        // edited values are lost when the fragment is restored after being purged
        // from memory.
    }


    ///
    // Inflate various Sections as per the given Attribute section
    ///
    private fun inflateAttributeSection(rootView: View) {
        val attrCnt = mAttributeSection!!.mAttributes!!.size
        var isLastAtr = false

        for (i in 0 until attrCnt) {
            val attribute = mAttributeSection!!.mAttributes!!.get(i)
            if (i == attrCnt - 1) {
                isLastAtr = true
            }
            inflateAttribute(attribute, isLastAtr, mSectionLayout, false, rootView)
        }
    }


    ///
    // Construct the layout for the given attribute.
    ///
    private fun inflateAttribute(attribute: Attribute, isLastAtr: Boolean, parent: LinearLayout?, retainValues: Boolean, rootView: View?) {
        val type = attribute.getType()
        val presentation = attribute.getPresentation()

        if (presentation?.isLayer == false) {
            if (type == Type.BOOLEAN) {
                inflateBooleanType(attribute, isLastAtr, parent, retainValues, rootView)
            } else if (type == Type.BOOLEAN_GROUP) {
                inflateBooleanGroup(attribute, parent, rootView)
            } else if (type == Type.INTEGER_GROUP || type == Type.RANGE_INTEGER) {
                inflateIntegerGroup(attribute, parent)
            } else if (type == Type.INTEGER) {
                inflateInteger(attribute, parent, rootView)
            } else if (type == Type.MORE_ATTRIBUTES_GROUP) {
                inflateMoreAttributesGroup(attribute, isLastAtr, parent, rootView)
            }
        } else {
            if (type == Type.BOOLEAN_GROUP) {
                inflateOverlay(attribute, parent, rootView)
            } else if (type == Type.MORE_ATTRIBUTES_GROUP) {
                inflateMoreAttributesGroup(attribute, isLastAtr, parent, rootView)
            }
        }
    }

    ///
    // Inflate the group attributes inside more attributes group
    ///
    private fun inflateMoreAttributesGroup(attribute: Attribute, isLastAtr: Boolean,
                                           parent: LinearLayout?, rootView: View?) {
        val moreAttributesGroup = attribute as MoreAttributesGroup
        val attributes = moreAttributesGroup.groupValues
        var showMoreAttributes = false

        val value = ""
        val id = attribute.getId()

        // as per new requirement no need to show the chevron if tapping on item needs to show the overlay

        val navigationItem = NavigationItem.build(this.context,
                R.style.NavigationItem)
                .setLeftView(attribute.getLabel()).setRightView(/*R.drawable.push_chevron*/"",
                        com.care.patternlib.R.style.NavigationItemImage_Medium).setDivider(false)

        if (mbIsUsedInJobDetailsFlow) {
            navigationItem.setDivider(false)
        } else {
            navigationItem.setDivider(true)
        }

        //dynamic_layout.setTag(id);
        parent!!.addView(navigationItem)
        val fm = activity.supportFragmentManager

        navigationItem.setOnClickListener {
            /*MoreAttributeFragment moreAttributeFragment = new MoreAttributeFragment(
        AttributeSectionFragment.this, TextUtils.isEmpty(mAttributeSection.mTitle) ? attribute.getLabel() : mAttributeSection.mTitle, attribute);
moreAttributeFragment.show(fm, "fragment_edit_name");*/
            showMoreAttributeInCareDialog(this@AttributeSectionFragment, if (TextUtils.isEmpty(mAttributeSection!!.mTitle)) attribute.getLabel()!! else mAttributeSection!!.mTitle, attribute)
        }

        // Read only mode
        if (!mIsEditing) {
            for (attr in attributes) {
                if (attr.toBoolean()) {
                    showMoreAttributes = true
                    break
                }
            }

            // Hide more attributes layout
            if (!showMoreAttributes) {
                val titleView = rootView!!.findViewById(R.id.title) as TextView
                var headerView: View? = null

                // Hide header view, title view and dynamic layout only if header view and title view are present in attribute section
                if (titleView != null) {
                    titleView.visibility = View.GONE
                    if (!(mbIsUsedInJobFlow || mbIsUsedInJobDetailsFlow)) {
                        if (headerView != null) {
                            headerView.visibility = View.GONE
                        }
                    }
                }
                //dynamic_layout.setVisibility(View.GONE);
                navigationItem.visibility = View.GONE
            }
        }
    }

    private fun showMoreAttributeInCareDialog(attributeSectionFragment: AttributeSectionFragment, title: String, attribute: Attribute) {
        val mTitle: String
        val mAttribute: Attribute
        val mBodyLayout: LinearLayout
        val mRootView: View
        val mAttributeSectionFragment: WeakReference<AttributeSectionFragment>?

        mAttributeSectionFragment = WeakReference(attributeSectionFragment)
        mTitle = title
        mAttribute = attribute

        val context = activity

        // Inflate the layout
        val layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        val layoutView: View? = null
        val careCheckBoxPreferences: CareCheckBox? = null

        val careDialog = CareDialog.build(context).addHeader(title,R.style.header_2)
                .addDescription("")
                .addActionButton("OK", com.care.patternlib.R.style.PrimaryButton_Small_Grey_SingleCta)
                .onPositive { dialog, which ->
                    if (mAttributeSectionFragment != null) {
                        val parent = mAttributeSectionFragment.get()

                        parent?.addCurrentAttributeValues()
                    }
                    dialog.dismiss()
                }

        mRootView = layoutInflater.inflate(R.layout.more_attribute_fragment, null)
        mBodyLayout = mRootView.findViewById(R.id.body_layout) as LinearLayout
        val ok = mRootView.findViewById(R.id.ok) as Button
        ok.visibility = View.GONE

        val moreAttributesGroup = mAttribute as MoreAttributesGroup
        val attributes = moreAttributesGroup.groupValues

        for (i in attributes.indices) {
            /*if (isLastAtr) {
                isLastAtr = (i == attributes.size() - 1);
            }*/

            val moreAttribute = attributes.get(i)

            if (mAttributeSectionFragment != null) {
                val parent = mAttributeSectionFragment.get()

                if (parent != null) {
                    if (mbIsUsedInJobDetailsFlow) {
                        if (moreAttribute.toBoolean()) {
                            parent.inflateAttribute(moreAttribute, /*isLastAtr*/ false, mBodyLayout,
                                    true, mRootView)
                        }
                    } else {
                        parent.inflateAttribute(moreAttribute, /*isLastAtr*/ false, mBodyLayout,
                                true, mRootView)
                    }
                }
            }
        }

        val linearLayout = LinearLayout(context)
        val viewParams = LinearLayout.LayoutParams(ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT))
        linearLayout.layoutParams = viewParams
        linearLayout.orientation = LinearLayout.VERTICAL

        /*View  view  = new View(context);
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, Utils.dipToPixels(40)));
        linearLayout.addView(view);*/
        linearLayout.addView(mRootView)

        careDialog.addView(linearLayout)
        careDialog.show()
    }

    class MoreAttributeFragment : DialogFragment {
        private lateinit var mTitle: String
        private lateinit var mAttribute: Attribute
        private var mBodyLayout: LinearLayout? = null
        private var mRootView: View? = null
        private  var mAttributeSectionFragment: WeakReference<AttributeSectionFragment>? = null

        private var isUsedInJobDetailsFlow: Boolean = false

        constructor() {
            // do nothing
        }

        @SuppressLint("ValidFragment")
        constructor(attributeSectionFragment: AttributeSectionFragment, title: String, attribute: Attribute) {
            mAttributeSectionFragment = WeakReference(attributeSectionFragment)
            mTitle = title
            mAttribute = attribute
        }

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            setStyle(DialogFragment.STYLE_NORMAL, R.style.HelpDialog)
        }

        override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
            super.onCreateView(inflater, container, savedInstanceState)
            if (arguments != null) {
                isUsedInJobDetailsFlow = arguments.getBoolean(IS_USED_FROM_JOB_DETAILS_FLOW)
            }
            // Inflate the layout for this fragment
            mRootView = inflater!!.inflate(R.layout.more_attribute_fragment, container, false)
            mBodyLayout = mRootView!!.findViewById(R.id.body_layout) as LinearLayout
            val ok = mRootView!!.findViewById(R.id.ok) as Button
            ok.setOnClickListener {
                if (mAttributeSectionFragment != null) {
                    val parent = mAttributeSectionFragment!!.get()

                    parent?.addCurrentAttributeValues()
                }

                dismiss()
            }
            return mRootView as View
        }

        override fun onActivityCreated(savedInstanceState: Bundle?) {
            var isLastAtr = false
            super.onActivityCreated(savedInstanceState)

            dialog.setTitle(mTitle)

            val moreAttributesGroup = mAttribute as MoreAttributesGroup
            val attributes = moreAttributesGroup.groupValues

            for (i in attributes.indices) {
                if (isLastAtr) {
                    isLastAtr = i == attributes.size - 1
                }

                val moreAttribute = attributes.get(i)

                if (mAttributeSectionFragment != null) {
                    val parent = mAttributeSectionFragment!!.get()

                    if (parent != null) {
                        if (isUsedInJobDetailsFlow) {
                            if (moreAttribute.toBoolean()) {
                                parent.inflateAttribute(moreAttribute, isLastAtr, mBodyLayout,
                                        true, mRootView)
                            }
                        } else {
                            parent.inflateAttribute(moreAttribute, isLastAtr, mBodyLayout,
                                    true, mRootView)
                        }
                    }
                }
            }
        }
    }


    ///
    // Inflate integer
    ///
    private fun inflateInteger(attribute: Attribute, parent: LinearLayout?, rootView: View?) {
        val params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
//        val viewsAttributeString = ViewsAttributeString(activity)
        val type = InputType.TYPE_CLASS_TEXT
        var navigationItem: NavigationItem? = null

//        viewsAttributeString.setIsEditing(mIsEditing)
//        viewsAttributeString.setTag(attribute.getId())
//        viewsAttributeString.setValueInputType(if (attribute.getType() == Type.INTEGER) InputType.TYPE_CLASS_NUMBER else type)
//        viewsAttributeString.configureView(attribute.getLabel(), attribute.getValue())

        if (mbIsUsedInJobFlow || mbIsUsedInJobDetailsFlow) {
            navigationItem = NavigationItem.build(this.context, R.style.NavigationItem)
                    .setLeftView(attribute.getLabel(), com.care.patternlib.R.style.NavigationItemText)
                    .setRightView(attribute.getValue(),com.care.patternlib.R.style.NavigationItemText_Dark)
                    .setDivider(false)
        }

        // Read only mode and attribute value flag is false
        if (!mIsEditing && attribute.toInt() == 0) {
            val titleView = rootView!!.findViewById(R.id.title) as TextView
            var headerView: View? = null

            // Hide header view, title view and dynamic layout only if header view and title view
            // are present in attribute section
            if (titleView != null) {
                // Hide title and header only if show title flag is false
                if (!mShowIntegerTitle) {
                    titleView.visibility = View.GONE
                    if (!(mbIsUsedInJobFlow || mbIsUsedInJobDetailsFlow)) {
                        if (headerView != null) {
                            headerView.visibility = View.GONE
                        }
                    }
                }
                // Hide the layout
//                viewsAttributeString.setVisibility(View.GONE)

                return
            }
        }

        // Store the view's reference so that updated attribute value can be collected later
        if (mbIsUsedInJobFlow || mbIsUsedInJobDetailsFlow) {
            mAttributeViewList!!.add(AttributeView(navigationItem!!, ViewType.VIEWS_ATTRIBUTE_STRING,
                    attribute))
            parent!!.addView(navigationItem, params)
        }
    }


    ///
    // Inflate section type one which has all boolean displayed in
    // checkbox one below another.
    ///
    private fun inflateBooleanType(attribute: Attribute, isLastAtr: Boolean, parent: LinearLayout?, retainValues: Boolean, rootView: View?) {
        Log.d("Debug", "inflateBooleanType: label: " + attribute.getLabel() + " retValues: " + retainValues)

        // Inflate the layout for this fragment
        val layoutInflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var layoutView: View? = null
        //Todo this check has to be removed when all the related screens are moved
        // TODO to patternLib
        if (mbIsUsedInJobFlow || mbIsUsedInJobDetailsFlow) {
            layoutView = layoutInflater.inflate(R.layout.attribute_boolean_new, null)
        }
        val dynamic_layout = layoutView as LinearLayout?
        var chkPreferences: CheckBox? = null
        var careCheckBoxPreferences: CareCheckBox? = null
        var divider: View? = null
        if (mbIsUsedInJobFlow || mbIsUsedInJobDetailsFlow) {
            careCheckBoxPreferences = layoutView!!.findViewById(R.id.checkbox) as CareCheckBox
            val value = java.lang.Boolean.parseBoolean(attribute.getValue())
            val drawableId = com.care.patternlib.R.drawable.list_item_check_image
            careCheckBoxPreferences.setLeftView(attribute.getLabel(), R.style.NavigationItemText_MaxWidth)

            //int drawableId = com.care.patternlib.R.drawable.list_item_check;
            careCheckBoxPreferences.isEnabled = mIsEditing
            careCheckBoxPreferences.isSelected = attribute.toBoolean()
            careCheckBoxPreferences.state = attribute.toBoolean()
            careCheckBoxPreferences.setDivider(false)

            val finalCareCheckBoxPreferences = careCheckBoxPreferences
            careCheckBoxPreferences.setOnClickListener { view ->
                (view as CareCheckBox).state = !view.state
                attribute.mValue = (java.lang.Boolean.toString(finalCareCheckBoxPreferences.state))
            }

            if (mbIsUsedInJobDetailsFlow) {
                careCheckBoxPreferences.setDrawable(drawableId,
                        com.care.patternlib.R.style.NavigationItemImage_Medium)
            }

            //Store the view's reference so that updated attribute value can be collected later
            mAttributeViewList!!.add(AttributeView(careCheckBoxPreferences, ViewType.CHECK_BOX,
                    attribute))

        }

        //in case of moreAttributes, the same view might be created more than once if displayed
        // in overlay and we need to restore the previously selected values
//        if (retainValues) {
//            if (mUpdatedAttributes!!.containsKey(attribute.getId())) {
//                val boolStr = mUpdatedAttributes!!.getValue(attribute.getId())
//                if (!(mbIsUsedInJobFlow || mbIsUsedInJobDetailsFlow)) {
//                    chkPreferences!!.isSelected = java.lang.Boolean.valueOf(boolStr[0])
//                } else {
//                    careCheckBoxPreferences!!.isSelected = java.lang.Boolean.valueOf(boolStr[0])
//                }
//            }
//        }

        //If its last attribute in that AttributeSection, then hide the divider
        if (isLastAtr == true) {
            if (!(mbIsUsedInJobFlow || mbIsUsedInJobDetailsFlow)) {
                divider!!.visibility = View.GONE
            }
        }

        dynamic_layout!!.isEnabled = mIsEditing

        // Read only mode and attribute value flag is false
        if (!mIsEditing && !attribute.toBoolean()) {
            val titleView = rootView!!.findViewById(R.id.title) as TextView
            var headerView: View? = null
//            if (!(mbIsUsedInJobFlow || mbIsUsedInJobDetailsFlow)) {
//                headerView = rootView.findViewById(R.id.header_view)
//            }

            // Hide header view, title view and dynamic layout only if header view and title view are present in attribute section
            if (titleView != null) {
                // Hide title and header only if show title flag is false
                if (!mShowBooleanTitle) {
                    titleView.visibility = View.GONE
                    if (!(mbIsUsedInJobFlow || mbIsUsedInJobDetailsFlow)) {
                        if (headerView != null) {
                            headerView.visibility = View.GONE
                        }
                    }
                }

                // Hide the layout
                dynamic_layout.visibility = View.GONE
            }
        }

        parent!!.addView(dynamic_layout)
    }


    ///
    // Inflate section type two which has boolean Group, displayed in checkbox in grid view.
    ///
    private fun inflateBooleanGroup(attribute: Attribute, parent: LinearLayout?, rootView: View?) {
        val dynamic_layout = LinearLayout(activity)
        var showBooleanGroup = false
        val attributes = (attribute as AttributeBooleanGroup).groupValues

        val leftPadding = dipToPixels(0f)
        val topPadding = dipToPixels(0f)
        val rightPadding = dipToPixels(0f)
        val bottomPadding = dipToPixels(0f)

        dynamic_layout.setPadding(leftPadding, topPadding, rightPadding, bottomPadding)


        val minHeight = dipToPixels(50f)
        dynamic_layout.minimumHeight = minHeight

        val customGridView = CustomGridView(activity)
        customGridView.isExpanded =(true)
        customGridView.setNumColumns(1)
        customGridView.setEnabled(mIsEditing)

        val newAttributes = ArrayList<Attribute>()

        // Read only mode and attributes are valid
        if (!mIsEditing && attributes != null) {
            for (attr in attributes) {
                if (attr.toBoolean()) {
                    showBooleanGroup = true
                    newAttributes.add(attr)
                }
            }

            val titleView = rootView!!.findViewById(R.id.title) as TextView
            var headerView: View? = null


            // Hide header view, title view and dynamic layout only if header view and title view are present in attribute section
            if (titleView != null) {
                // Hide title and header only if show boolean flag is false
                if (!showBooleanGroup) {
                    titleView.visibility = View.GONE
                    if (!(mbIsUsedInJobFlow || mbIsUsedInJobDetailsFlow)) {
                        if (headerView != null) {
                            headerView.visibility = View.GONE
                        }
                    }

                    customGridView.setAdapter(null)
                    customGridView.setVisibility(View.GONE)

                    // Hide the layout
                    dynamic_layout.visibility = View.GONE

                    return
                }
            }
        } else {
            newAttributes.addAll(attributes)
        }

        val gridviewAdapter = GridviewAdapter(activity, attribute.getId()!!, newAttributes, mIsEditing, attribute.getMultiSelect(),
                mbIsUsedInJobFlow, mbIsUsedInJobDetailsFlow)
        customGridView.setAdapter(gridviewAdapter)

        customGridView.setLayoutParams(ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT))
        dynamic_layout.addView(customGridView)
        dynamic_layout.isEnabled = mIsEditing

        parent!!.addView(dynamic_layout)

        //Store the view's reference so that updated attribute value can be collected later
        mAttributeViewList!!.add(AttributeView(customGridView, ViewType.GRID_VIEW, attribute))
    }


    ///
    // Inflate section type three which has integer, displayed in Rangeseekbar with single/double thumb
    ///
    private fun inflateIntegerGroup(attribute: Attribute, parent: LinearLayout?) {
        val attributeIntegerGroup = attribute as AttributeIntegerGroup

        val dynamic_layout = LinearLayout(activity)

        val leftPadding = dipToPixels(0f)
        val topPadding = dipToPixels(10f)
        val rightPadding = dipToPixels(0f)
        val bottomPadding = dipToPixels(10f)

        dynamic_layout.setPadding(leftPadding, topPadding, rightPadding, bottomPadding)
        dynamic_layout.setBackgroundResource(R.drawable.corners_squared_white_bg)

        val minHeight = dipToPixels(50f)
        dynamic_layout.minimumHeight = minHeight

//        val rangeBar: RangeBar
//        val labels = arrayOfNulls<String>(attributeIntegerGroup.getGroupValues().size)
//
//        var i = 0
//        for (value in attributeIntegerGroup.getGroupValues()) {
//            labels[i] = "" + value
//            i++
//        }
//
//        if (attributeIntegerGroup.hasMore()) {
//            labels[i - 1] = labels[i - 1] + "+"
//        }
//
//        val absoluteMin = 0
//        val absoluteMax = labels.size - 1
//
//        rangeBar = RangeBar(activity)
//        rangeBar.setLabelArray(labels)
//        rangeBar.setLabelPadding(dipToPixels(10f).toFloat())
//
//        //Store the view's reference so that updated attribute value can be collected later
//        mAttributeViewList!!.add(AttributeView(rangeBar, ViewType.RANGE_SEEK_BAR, attribute))
//
//        val defMin = if (attribute.getSelectedDefMin() != 0) attribute.getSelectedDefMin() else 1
//        val defMax = if (attribute.getSelectedDefMax() != 0) attribute.getSelectedDefMax() else 1
//        val interval = attribute.getInterval()
//
//        //Set default values
//        if (attribute.getType() == Type.RANGE_INTEGER) {
//            rangeBar.setThumbIndices(defMin / interval - 1, defMax / interval - 1)
//        } else {
//            rangeBar.setThumbIndices(0, defMax / interval - 1)
//            rangeBar.setSingleThumbBar(true)
//        }
//
//        rangeBar.setLabelTextSize(resources.getDimension(R.dimen.range_bar_label_text_size))
//        rangeBar.setLabelTextColor(resources.getColor(R.color.cc_field_color_dark))
//        rangeBar.setEnabled(mIsEditing)
//
//        dynamic_layout.addView(rangeBar)
//        dynamic_layout.isEnabled = mIsEditing
//        parent!!.addView(dynamic_layout)
    }


    ///
    // Inflate section type four which has boolean Group, and presentation as overlay.
    ///
    private fun inflateOverlay(attribute: Attribute, parent: LinearLayout?, rootView: View?) {
        var value = ""
        val id = attribute.getId()
        var showAttributes = false

        val navigationItem = NavigationItem.build(this.context,
                R.style.NavigationItem)

        if (mbIsUsedInJobDetailsFlow) {
            navigationItem.setDivider(false)
        } else {
            navigationItem.setDivider(true)
        }

        // Assemble a boolean-group's value.
        if (attribute is AttributeBooleanGroup) {
            for (attr in attribute.groupValues) {
                if (attr.getValue() == "true") {
                    value += attr.getLabel() + ","
                    showAttributes = true
//                    mUpdatedAttributes!!.put(attribute.getId(), attr.getId())
                }
            }

            // To remove last ',' from string.
            if (value.length > 0) {
                value = value.substring(0, value.length - 1)
            }
        }

        // set the label.
        if (attribute.getPresentation()?.showLabel == false) {
            //show default value as label, ex: 'English' is shown in label field of UI component
            navigationItem.setLeftView(value, R.style.NavigationItemText)
        } else {
            //show label
            navigationItem.setLeftView(attribute.getLabel(), R.style.NavigationItemText)
        }
        //set the value.
        if (attribute.getPresentation()?.showLabel == true) {
            //show default value at value textView
            if (mbIsUsedInJobFlow) {
                // as per new requirement no need to show the chevron if tapping on item needs to show the overlay
                // navigationItem.setRightView(R.drawable.push_chevron, R.style.NavigationItemImage);
                navigationItem.setRightView("", com.care.patternlib.R.style.NavigationItemImage)
                navigationItem.setCenterView(value, com.care.patternlib.R.style.NavigationItemText_Dark_CenterRight)
            } else {
                navigationItem.setRightView(value, com.care.patternlib.R.style.NavigationItemText_Dark_Ellipsize)
            }
        }
        setListener(navigationItem, attribute)
        // Hide the label and value if value is zero and it is children attribute
        if (hideChildrenAttribute(attribute.getId()!!, value)) {
            // Hide the layout
            navigationItem.visibility = View.GONE
        }
        // Read only mode and we need not show attributes
        if (!mIsEditing && !showAttributes) {
            val titleView = rootView!!.findViewById(R.id.title) as TextView
            var headerView: View? = null
            // Hide header view, title view and dynamic layout only if header view and title view are present in attribute section
            if (titleView != null) {
                // Hide title and header
                titleView.visibility = View.GONE
                if (!(mbIsUsedInJobFlow || mbIsUsedInJobDetailsFlow)) {
                    if (headerView != null) {
                        headerView.visibility = View.GONE
                    }
                }
                // Hide the layout
                navigationItem.visibility = View.GONE
                return
            }
        }
        parent!!.addView(navigationItem)
    }

    private fun setListener(navigationItem: NavigationItem, attribute: Attribute) {
        navigationItem.setOnClickListener(View.OnClickListener { v ->
            val id = v.tag as String?
            var tag = id + "_value"
            var txtView: TextView? = null
            if (mbIsUsedInJobFlow) {
                txtView = (v as NavigationItem).centerView as TextView
            } else {
                txtView = (v as NavigationItem).rightView as TextView
            }

            tag = id + "_label"
            val titleView = v.leftView as TextView

//            if (attribute.getMultiSelect()) {
//                //TODO to remove last variable
//                mOverlayDialog = MultiSelectDialogUtil(activity,
//                        attribute as AttributeBooleanGroup, mIsEditing, null, true)
//
//                //Store the view's reference so that updated attribute value can be collected later
//                mAttributeViewList!!.add(AttributeView(mOverlayDialog, ViewType.MULTI_SELECT_DIALOG, attribute))
//
//
//                // Here attribute id is checked bcoz, we have to set the selected value to
//                // title textView in other place it will be value textView.
//                if (attribute.getPresentation().getShowLabel() == false) {
//                    // mOverlayDialog.setTitle(mAttributeSection.mTitle);
//                    mOverlayDialog!!.setTitle(attribute.getLabel())
//                    mOverlayDialog!!.showDialog(getString(R.string.ok), getString(R.string.cancel), titleView)
//                } else {
//                    mOverlayDialog!!.showDialog(getString(R.string.ok), getString(R.string.cancel), txtView)
//                }
//            } else {
//                // Read only mode
//                if (hideSingleSelectDialog(attribute.getId())) {
//                    return@OnClickListener
//                }
//
//                val singleSelectDialogUtil = SingleSelectDialogUtil(attribute as AttributeBooleanGroup, mUpdatedAttributes)
//                var title = titleView.text.toString()
//                if (attribute.getPresentation().getShowLabel() == false) {
//                    title = attribute.getLabel()
//                }
//                singleSelectDialogUtil.showDialog(activity, title, txtView)
//
//                //Store the view's reference so that updated attribute value can be collected later
//                mAttributeViewList!!.add(AttributeView(singleSelectDialogUtil, ViewType.SINGLE_SELECT_DIALOG, attribute))
//            }
        })
    }


    ///
    // Get the current attribute values so that the changes can be submitted to the server.
    ///
    fun addCurrentAttributeValues() {
        // TODO:Review mUpdatedAttributes ought to be removed from the class. This method
        // gathers the current values, so its not needed. Refactor the
        // single-select and multi-select to not require odd ball handling. For example,
        // after the dialog is dismissed, stash the values in AttributeView.mOverlayValue
        // (of type UpdatedAttributes).

        // Iterate over the attributes and add their current values to the UpdatedAttributes.
        val attrCnt = mAttributeViewList!!.size

//        for (i in 0 until attrCnt) {
//            val attributeView = mAttributeViewList!![i]
//
//            if (attributeView.mViewType == ViewType.CHECK_BOX) {
//                val fragmentActivity = activity
//                if (mbIsUsedInJobFlow || mbIsUsedInJobDetailsFlow) {
//                    mUpdatedAttributes!!.replace(attributeView.mAttribute.getId(),
//                            java.lang.Boolean.toString((attributeView.mView as CareCheckBox).state))
//                    Log.d("Debug", "id: " + attributeView.mAttribute.getId() + " value: " +
//                            java.lang.Boolean.toString((attributeView.mView as CareCheckBox).state))
//                    logEvent(attributeView.mAttribute.getLabel(),
//                            if ((attributeView.mView as CareCheckBox).state) "Yes" else "No")
//                } else {
//                    mUpdatedAttributes!!.replace(attributeView.mAttribute.getId(),
//                            java.lang.Boolean.toString((attributeView.mView as CheckBox).isChecked))
//                    Log.d("Debug", "id: " + attributeView.mAttribute.getId() + " value: " +
//                            java.lang.Boolean.toString((attributeView.mView as CheckBox).isChecked))
//                    logEvent(attributeView.mAttribute.getLabel(),
//                            if ((attributeView.mView as CheckBox).isChecked) "Yes" else "No")
//                }
//            } else if (attributeView.mViewType == ViewType.VIEWS_ATTRIBUTE_STRING) {
//                if (mbIsUsedInJobFlow || mbIsUsedInJobDetailsFlow) {
//                    val editText = (attributeView.mView as NavigationItem).rightView as TextView
//                    var value: String? = if (editText.text != null) editText.text.toString() else ""
//                    value = if (value == null) "" else value
//                    mUpdatedAttributes!!.replace(attributeView.mAttribute.getId(), value)
//                } else {
//                    mUpdatedAttributes!!.replace(attributeView.mAttribute.getId(),
//                            (attributeView.mView as ViewsAttributeString).getValue())
//                }
//            } else if (attributeView.mViewType == ViewType.GRID_VIEW) {
//                val customGridView = attributeView.mView as CustomGridView
//                val gridviewAdapter = customGridView.getAdapter() as GridviewAdapter
//                //Remove old values
//                mUpdatedAttributes!!.remove(gridviewAdapter.attributeId)
//                val itemCnt = gridviewAdapter.count
//
//                for (pos in 0 until itemCnt) {
//                    val attribute = gridviewAdapter.getItem(pos)
//                    if (attribute.toBoolean()) {
//                        val key = gridviewAdapter.attributeId
//                        mUpdatedAttributes!!.put(key, attribute.getId())
//                        logEvent(key.substring(0, 1).toUpperCase() + key.substring(1), attribute.getId())
//                        //Log.d("Debug", "id: " + gridviewAdapter.getAttributeId() + " value: " + attribute.getId());
//                    }
//                }
//            } else if (attributeView.mViewType == ViewType.SINGLE_SELECT_DIALOG) {
//                //Log.d("Debug","for SSDU from addCurrentAttribute() method");
//                //Do nothing bcoz updated attribute is updated inside singleSelectDialog component
//            } else if (attributeView.mViewType == ViewType.RANGE_SEEK_BAR) {
//                val rangeBar = attributeView.mView as RangeBar
//                val labels = rangeBar.getLabelArray()
//                val minValue = rangeBar.getLeftValue().replace("+", "")
//                val maxValue = rangeBar.getRightValue().replace("+", "")
//
//                if (attributeView.mAttribute.getType() == Type.RANGE_INTEGER) {
//                    mUpdatedAttributes!!.replace(attributeView.mAttribute.getId(), minValue + "," + maxValue)
//
//                    // TODO:Review This is no longer valid to associate this with provider enrollment.
//                    logEvent(attributeView.mAttribute.getLabel(), minValue + "," + maxValue)
//                    Log.d("Debug", "id: " + attributeView.mAttribute.getId() + " value: " + minValue + "," + maxValue)
//                } else {
//                    mUpdatedAttributes!!.replace(attributeView.mAttribute.getId(), maxValue)
//
//                    // TODO:Review This is no longer valid to associate this with provider enrollment.
//                    logEvent(attributeView.mAttribute.getLabel(), maxValue)
//                    Log.d("Debug", "id: " + attributeView.mAttribute.getId() + " value: " + maxValue)
//                }
//            } else if (attributeView.mViewType == ViewType.MULTI_SELECT_DIALOG) {
//                //Log.d("Debug","for MSDU from addCurrentAttribute() method");
//                mUpdatedAttributes!!.remove(attributeView.mAttribute.getId())
//                (attributeView.mView as MultiSelectDialogUtil).updateAttributes(mUpdatedAttributes)
//            }
//        }
    }

    ///
    // An adaptor for inline boolean-groups and alike.
    ///
    class GridviewAdapter///
    // Constructor
    ///
    (private val mParentActivity: Activity, val attributeId: String, ///
            // Member variables
            ///
     private val mList: List<Attribute>?,
     private val mIsEditable: Boolean, multiSelect: Boolean, private val mbIsUsedInJobFlow: Boolean,
     private val mbIsUsedInJobDetailsFlow: Boolean) : BaseAdapter() {
        private var mInitialValues = false
        private var mMultiSelect = false

        init {
            mMultiSelect = multiSelect
        }


        ///
        // Return the number of items.
        ///
        override fun getCount(): Int {
            return mList?.size ?: 0
        }


        ///
        // Get the item at the given position.
        ///
        override fun getItem(position: Int): Attribute {
            return mList!![position]
        }


        ///
        // Get the item's id at the given position.
        ///
        override fun getItemId(position: Int): Long {
            return 0
        }


        ///
        // Construct the view at position.
        ///
        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var convertView = convertView
            val customGridView = parent as CustomGridView
            val checkBox: CheckBox
            val inflator = mParentActivity.layoutInflater

            // Construct a new view instance.
            if (convertView == null) {
                if (mbIsUsedInJobFlow || mbIsUsedInJobDetailsFlow) {
                    convertView = inflator.inflate(R.layout.attribute_grid_view_item_new, null)
                }
            }
            if (mbIsUsedInJobFlow || mbIsUsedInJobDetailsFlow) {
                val careCheckBoxPreferences = convertView!!.findViewById(R.id.grid_chk_box) as CareCheckBox
                val drawableId = com.care.patternlib.R.drawable.list_item_check_image
                careCheckBoxPreferences.setLeftView(mList!![position].getLabel())
                careCheckBoxPreferences.isEnabled = mIsEditable
                careCheckBoxPreferences.isSelected = mList[position].toBoolean()
                careCheckBoxPreferences.setDivider(false)
                if (mbIsUsedInJobDetailsFlow) {
                    careCheckBoxPreferences.setDrawable(drawableId,
                            com.care.patternlib.R.style.NavigationItemImage_Medium)
                }

                careCheckBoxPreferences.setOnClickListener(View.OnClickListener {
                    mInitialValues = false//Once user start changing the value, default
                    // values are gone.
                    val attribute = mList[position]

                    careCheckBoxPreferences.state = !careCheckBoxPreferences.state

                    if (careCheckBoxPreferences.state == false && mMultiSelect == false) {
                        //Do not allow the user to deselect all the items.
                        careCheckBoxPreferences.state = true
                        return@OnClickListener
                    }

                    attribute.mValue = (java.lang.Boolean.toString(careCheckBoxPreferences.state))

                    //Uncheck other items if multiSelect is false
                    if (careCheckBoxPreferences.state == true && mMultiSelect == false) {
                        for (attr in mList) {
                            //If its same check box
                            if (attribute.getId() === attr.getId()) {
                                continue
                            }

                            if (attr.toBoolean() == true) {
                                attr.mValue = ("false")
                            }
                        }

                        val gridAdapter = GridviewAdapter(mParentActivity, attributeId,
                                mList, mIsEditable, false, mbIsUsedInJobFlow, mbIsUsedInJobDetailsFlow)
                        customGridView.setAdapter(gridAdapter)
                    }
                })

            } else {
                checkBox = convertView!!.findViewById(R.id.grid_chk_box) as CheckBox

                checkBox.text = mList!![position].getLabel()
                checkBox.isChecked = mList[position].toBoolean()
                checkBox.isEnabled = mIsEditable
                checkBox.tag = mList[position].getId()

                checkBox.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
                    mInitialValues = false//Once user start changing the value, default values are gone.
                    val attribute = mList[position]

                    if (isChecked == false && mMultiSelect == false) {
                        //Do not allow the user to deselect all the items.
                        buttonView.isChecked = true
                        return@OnCheckedChangeListener
                    }

                    attribute.mValue = (java.lang.Boolean.toString(isChecked))

                    //Uncheck other items if multiSelect is false
                    if (isChecked && mMultiSelect == false) {
                        for (attr in mList) {
                            //If its same check box
                            if (attribute.getId() === attr.getId()) {
                                continue
                            }

                            if (attr.toBoolean() == true) {
                                attr.mValue =("false")
                            }
                        }

                        val gridAdapter = GridviewAdapter(mParentActivity, attributeId, mList, mIsEditable, false,
                                mbIsUsedInJobFlow, mbIsUsedInJobDetailsFlow)
                        customGridView.setAdapter(gridAdapter)
                    }
                })
            }
            return convertView
        }
    }

    fun logEvent(eventAction: String, eventValue: String) {
        val tag = "Provider Enrollment"
//        if (Session.singleton().getIsEnrolling()) {
//            if (Session.singleton().getMemberType() == Session.MEMBER_TYPE_SITTER) {
//                GoogleLogger.logEvent(tag, eventAction, eventValue, 0)
//            }
//        }
    }


    ///
    // Return true if single select dialog needs to be hidden for read only mode
    ///
    private fun hideSingleSelectDialog(id: String): Boolean {
        return !mIsEditing && (id == "ccAgeGroups-CHLDAGEGP001" || id == "ccAgeGroups-CHLDAGEGP002" ||
                id == "ccAgeGroups-CHLDAGEGP003" || id == "ccAgeGroups-CHLDAGEGP004" ||
                id == "ccAgeGroups-CHLDAGEGP005")
    }


    ///
    // Return true if label and value need to be hidden for read only mode
    ///
    private fun hideChildrenAttribute(id: String, value: String): Boolean {
        return !mIsEditing && value == "0" && (id == "ccAgeGroups-CHLDAGEGP001" || id == "ccAgeGroups-CHLDAGEGP002" ||
                id == "ccAgeGroups-CHLDAGEGP003" || id == "ccAgeGroups-CHLDAGEGP004" ||
                id == "ccAgeGroups-CHLDAGEGP005")
    }

    companion object {
        ///
        // Consts
        ///
        private val ATTRIBUTE_SECTION = "attributeSection"
        private val UPDATED_ATTRIBUTE = "updatedAattribute"
        private val IS_EDITING = "isEditing"
        private val IS_JOB_ACTIVITY = "isJobActivity"
        private val IS_JOB_DETAILS_ACTIVITY = "isJobDetailsActivity"
        private val FIRST_FRAGMENT_ID = 0xA

        val IS_USED_FROM_JOB_FLOW = "isUsedInJobFlow"
        val IS_USED_FROM_JOB_DETAILS_FLOW = "isUsedInJobDetailsFlow"


        ///
        // Add the attribute section list
        ///
        fun addAttributeSections(parent: AppCompatActivity, rootLayout: LinearLayout,
                                 AttributeSectionsList: List<AttributeSection>,
                                 editable: Boolean, isFromPAJ: Boolean, isFromJobDetails: Boolean,
                                 isFromRecJobDetails: Boolean, additionalRequirements: String?) {
            var i = 0
            Log.d("Debug", "addAttributeSections() method, size: " + AttributeSectionsList.size)
            val fragmentTransaction = parent.supportFragmentManager.beginTransaction()

            for (section in AttributeSectionsList) {
                val frameLayout: FrameLayout
                var fragment: AttributeSectionFragment?

                // Add a container for the fragment.
                frameLayout = FrameLayout(parent)
                frameLayout.id = FIRST_FRAGMENT_ID + i
                rootLayout.addView(frameLayout)

                // If need be create a new Fragment to be placed in the activity layout. If the
                // activity is being restored the fragments will already exist and the activity
                // will properly attach them to the recreated frameLayouts (using the id).

                if (parent.supportFragmentManager.findFragmentById(FIRST_FRAGMENT_ID + i) == null) {
                    if (isFromRecJobDetails && (section.mTitle.equals("certifications", ignoreCase = true) || section.mTitle.equals("Subject Areas", ignoreCase = true))) {
                        fragment = AttributeSectionFragment(section, editable, isFromPAJ,
                                isFromJobDetails,
                                additionalRequirements)
                    } else {
                        fragment = AttributeSectionFragment(section, editable, isFromPAJ,
                                isFromJobDetails, null)
                    }
                    fragmentTransaction.add(FIRST_FRAGMENT_ID + i, fragment, "fragment_$i")
                }
                else{
                    fragment = parent.supportFragmentManager.findFragmentById(FIRST_FRAGMENT_ID + i) as AttributeSectionFragment
                    fragmentTransaction.add(FIRST_FRAGMENT_ID + i, fragment, "fragment_$i")

                }
                i++
                Log.d("Debug", "added fragment : " + i + " type: " + section.mTitle)
            }

            fragmentTransaction.commit()
        }


        ///
        // Remove the attribute section list
        ///
        fun removeAttributeSections(parent: AppCompatActivity?, rootLayout: LinearLayout?, attributeSectionsList: List<AttributeSection>?) {
            var i = 0

            // Sanity check
            if (attributeSectionsList == null || parent == null || rootLayout == null) {
                return
            }

            // TODO:Review do beginTransaction and commit outside of the loop.
            for (section in attributeSectionsList) {
                val fragment: AttributeSectionFragment?

                // Remove the fragments
                fragment = parent.supportFragmentManager.findFragmentByTag("fragment_$i") as AttributeSectionFragment

                if (fragment != null) {
                    val fragmentTransaction = parent.supportFragmentManager.beginTransaction()

                    fragmentTransaction.remove(fragment)
                    fragmentTransaction.commit()
                }

                // Remove the fragment's container
                val frameLayout = rootLayout.findViewById(FIRST_FRAGMENT_ID + i) as FrameLayout

                if (frameLayout != null) {
                    rootLayout.removeView(frameLayout)
                }

                i++
            }
        }

        fun dipToPixels(sizeInDips: Float): Int {
            val metrics = Resources.getSystem().displayMetrics

            return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, sizeInDips, metrics).toInt()
        }
    }
}

package com.example.searchscreen

import java.util.*

data class JobDetails(val jobTitle:String,val city : String,val jobZip:String,
                      val jobType:String,val jobCategory: String,val jobPostDate:String,
                      val jobStartDate : String,val postJobUntil:String,val description:String,
                      val jobId:Long,val jobStatus:String,val isReceivingApplications:Boolean,
                      val isDateNightJob:Boolean,val isTransportationJob:Boolean,
                      val transportationNumber:Long,val isFeatured:Boolean,val isPostedFromHKWidget:Boolean,
                      val isNew:Boolean,val applicationLimitMet:Boolean,val isFullTime:Boolean,
                      val hourlyRateFrom:String,val hourlyRateTo:String,val nnaJob:Boolean,val serviceId:String,
                      val serviceName:String,val distance:Long,val distanceText:String,val cannotApplyReason:String,
                      val coverLetterTemplate:String,var flatRate:Int?,var jobEndDate:String?,var appliedOn:String?,var canProviderApply:String?,
                      var experience:String?,var education:String?,var companyName:String,var companyInfo:String?,var additionalRequirements:String,
                      var jobStartDate2:Date?,var jobEndDate2:Date?,var appliedOn2 : Date?,var postJobUntil2:Date?,var jobPostDate2:Date?,val state:String,var listOfAttributes:List<AttributeSection>?,
                      var imageUrl:String?,var name:String?,var postedByMemberId:Int,var memberType:String?,var memberSubscriptionType:String?,var availability : Array<BooleanArray>?)
package com.example.searchscreen

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.Serializable
import java.util.ArrayList

class AttributeSection : Serializable {

    /**
     * Public properties.
     */
    var mTitle: String
    val mAttributes: List<Attribute>?

    /**
     * Constructor
     */
    @Throws(JSONException::class)
    constructor(json: JSONObject) {
        mTitle = json.getString("title")
        mAttributes = getServerAttributeList(json.getJSONArray("attributes"))
    }

    @Throws(JSONException::class)
    private fun getServerAttributeList(jsonArray: JSONArray?): List<Attribute>? {
        var serverAttributeList: MutableList<Attribute>? = null

        if (jsonArray != null) {
            serverAttributeList = ArrayList<Attribute>()

            for (i in 0 until jsonArray.length()) {
                val serverAttribute = Attribute.serverAttributeFromJson(jsonArray.get(i) as JSONObject)
                serverAttributeList.add(serverAttribute)
            }
        }
        return serverAttributeList
    }

    companion object {
        internal const val serialVersionUID = 1L
    }
}

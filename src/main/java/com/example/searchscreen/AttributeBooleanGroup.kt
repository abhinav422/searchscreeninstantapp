package com.example.searchscreen

import org.json.JSONException
import org.json.JSONObject
import java.util.ArrayList

class AttributeBooleanGroup
@Throws(JSONException::class)
internal constructor(jsonObject: JSONObject) : Attribute(jsonObject) {

    /**
     * Private variables.
     */
    private val mGroupValues: ArrayList<Attribute>

    /**
     * Get the boolean values.
     */
    val groupValues: List<Attribute>
        get() = mGroupValues

    init {
        if (getType() != Type.BOOLEAN_GROUP) {
            throw RuntimeException("Wrong type")
        }

        // Extract the boolean list too.
        val values = jsonObject.getJSONArray("value")

        mGroupValues = ArrayList(values.length())

        for (i in 0 until values.length()) {
            val value = values.getJSONObject(i)
            val attribute: Attribute

            attribute = Attribute(value)

            if (attribute.getType() != Type.BOOLEAN) {
                throw RuntimeException("Wrong type")
            }

            mGroupValues.add(attribute)
        }
    }

    companion object {
        internal const val serialVersionUID = 1L
    }
}

package com.example.searchscreen

import dagger.Component
import javax.inject.Singleton

@Component(modules = [AppComponentModule::class])
@Singleton
interface AppComponent{
     fun inject(searchScreenApplication: SearchScreenApplication)
}
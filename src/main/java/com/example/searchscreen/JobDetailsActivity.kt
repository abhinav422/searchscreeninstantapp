package com.example.searchscreen

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.v4.content.ContextCompat

import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.ScrollView
//import com.example.searchscreen.patternlib.CareTextBlock
import java.util.ArrayList
import com.care.patternlib.CircularImageView
import com.care.patternlib.CustomTextView
import com.care.patternlib.NavigationItem
import java.util.*

class JobDetailsActivity : AppCompatActivity() {

    lateinit var jobDetailsViewModel: JobDetailsViewModel
    var jobId: Long = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.job_details_new)
        if (intent.extras != null) {
            jobId = intent.extras.getLong("jobId", 0)
        }
        if (jobId == 0L) {
            var uri = intent.data
            var path = uri.path.toCharArray()
            var counter = 0
            var job = ""
            for (i in 0 until path.size) {
                var c = path[i]
                if (counter == 5) {
                    if (c != '-') {
                        job += c
                    } else {
                        break
                    }
                }
                if (c == '/') {
                    counter++
                }
            }
            jobId = job.toLong()

        }
        jobDetailsViewModel = ViewModelProviders.of(this).get(JobDetailsViewModel::class.java)
        jobDetailsViewModel.jobDetailsLiveData.observe(this, object : Observer<JobDetails> {
            override fun onChanged(t: JobDetails?) {
                findViewById<android.widget.ProgressBar>(R.id.pb).visibility = View.GONE
                findViewById<ScrollView>(R.id.jobDetailRootView).visibility = View.VISIBLE
                initViews2(t!!)
            }
        })
        jobDetailsViewModel.startRequest(jobId)
        title = "Job Details"
    }

    private fun initViews2(jobInfo: JobDetails) {
        val generalLayout = findViewById<LinearLayout>(R.id.general_layout)
        val generalHeader = findViewById<CustomTextView>(R.id.general_header_list)
        val companyInfoHeader = findViewById<CustomTextView>(R.id.company_info_header)
        val companyInfoContent = findViewById<CustomTextView>(R.id.company_info_content)

        // Remove all views
        generalLayout.removeAllViews()

        // Add the header views now
        generalLayout.addView(generalHeader)

        if (jobInfo.jobType.equals("R")) {
            (supportFragmentManager.findFragmentById(R.id.job_availability_fragment) as JobDetailAvailabilityFragment).updateViews(jobInfo, AvailabilityFragmentNew.Mode.VIEW_MODE)
        } else {
            findViewById<LinearLayout>(R.id.availability_layout).visibility = View.GONE
        }
        if (jobInfo.jobPostDate2 != null) {
            val posted = getString(R.string.posted) + " " +
                    java.text.SimpleDateFormat("MMMM dd, yyyy").format(jobInfo.jobPostDate2)
            (findViewById<CustomTextView>(R.id.posted_block)).text = posted
        }
        (findViewById<CustomTextView>(R.id.posted_block)).textSize = 13f
        if (jobInfo.jobTitle != "") {
            (findViewById<CustomTextView>(R.id.title)).text = jobInfo.jobTitle.substring(0, 1).toUpperCase() + jobInfo.jobTitle.substring(1)
        }

        if (jobInfo.jobStartDate2 != null) {
            (findViewById<CustomTextView>(R.id.day_text)).text = java.text.SimpleDateFormat("EEEE, MMMM dd, yyyy").format(jobInfo.jobStartDate2)
            (findViewById<CustomTextView>(R.id.day_text)).visibility = View.VISIBLE
        }

        val address = jobInfo.city + ", " + jobInfo.state + " "
        val distance: String = if (jobInfo.distance > 0) {
            jobInfo.distance.toString() + resources.getString(R.string.job_details_mi_away)
        } else {
            resources.getString(R.string.job_details_less_1_mi)
        }

        findViewById<CircularImageView>(R.id.profile_img).setDefaultProfileBitmap(jobInfo.name)
        (findViewById<CustomTextView>(R.id.address_text)).text = address
        findViewById<CustomTextView>(R.id.distance_text).text = distance
        if (jobInfo.jobCategory.equals("COMPANY", ignoreCase = true)) {
            findViewById<CustomTextView>(R.id.company_name).setText(jobInfo.companyName)
        }
        val status: String?
        //Post until date
        if (jobInfo.jobStatus.equals("C", ignoreCase = true)) {
            status = resources.getString(R.string.job_details_status_closed)
            findViewById<CustomTextView>(R.id.status).setTextAppearance(R.style.status_flag_3)
        } else {
            status = resources.getString(R.string.job_details_status_open)
        }
        findViewById<CustomTextView>(R.id.status).text = status

        findViewById<com.care.patternlib.CareTextBlock>(R.id.description_block).value = (jobInfo.description)

        val attributes = ArrayList<JobAttributes>()
        if (jobInfo.jobCategory.equals("COMPANY", ignoreCase = true)) run {
            (findViewById<CustomTextView>(R.id.date_time_header)).setText(R.string.job_schedule)
            if (jobInfo.companyInfo != null && jobInfo.companyInfo != null &&
                    !jobInfo.companyInfo!!.isEmpty()) {
                companyInfoHeader.visibility = View.VISIBLE
                companyInfoContent.visibility = View.VISIBLE
                companyInfoContent.text = jobInfo.companyInfo
            }
            if (jobInfo.experience != null) {
                attributes.add(JobAttributes(getString(R.string.rec_experience),
                        jobInfo.experience + " " + getString(R.string.rec_years)))
            }
            if (jobInfo.education != null) {
                attributes.add(JobAttributes(getString(R.string.rec_education),
                        jobInfo.education!!))
            }
            //salary/rate
            // Show flat rate if flat rate is available
            if (jobInfo.flatRate != null && jobInfo.flatRate!! > 0) {
                attributes.add(JobAttributes(getString(R.string.rec_annual_salary),
                        "$" + jobInfo.flatRate))
            }
        }
        else run {

            attributes.add(JobAttributes(getString(R.string.how_often_colon),
                    if (jobInfo.jobType == "R") "Recurring" else "One Time"))

            if (jobInfo.jobStartDate != null) {
                val dateFormat = if (jobInfo.jobType == "R")
                    java.text.SimpleDateFormat("EEE, MMM dd yyyy").format(jobInfo.jobStartDate2)
                else
                    java.text.SimpleDateFormat("EEE, MMM dd yyyy hh:mm aaa").format(jobInfo.jobStartDate2)
                attributes.add(JobAttributes(getString(R.string.start_date), dateFormat))
            }

            if (jobInfo.jobEndDate != null) {
                val dateFormat = if (jobInfo.jobType == "R")
                    java.text.SimpleDateFormat("EEE, MMM dd yyyy").format(jobInfo.jobEndDate2)
                else
                    java.text.SimpleDateFormat("EEE, MMM dd yyyy hh:mm aaa").format(jobInfo.jobEndDate2)
                attributes.add(JobAttributes(getString(R.string.end_date), dateFormat))
            } else {
                attributes.add(JobAttributes(getString(R.string.end_date), "None"))
            }

            // Show flat rate if flat rate is available
            if (jobInfo.flatRate != null && jobInfo.flatRate!! > 0) {
                attributes.add(JobAttributes(getString(R.string.flat_rate), "$" + jobInfo.flatRate))
            }
        }
        Log.d("attributesSize", attributes.size.toString())
        if (!attributes.isEmpty()) {
            generalLayout.visibility = View.VISIBLE

            for (i in attributes.indices) {
                val navigationItem = NavigationItem.build(this, R.style.NavigationItem)
                        .setLeftView(attributes[i].label, R.style.NavigationItemText)
                        .setRightView(attributes[i].value, R.style.NavigationItemText_TwoLine_DarkWithPaddingLeftExtra).setDivider(
                                ContextCompat.getColor(this, R.color.border_color))
                generalLayout.addView(navigationItem)
            }
        }
        val dynamicLayout = findViewById<LinearLayout>(R.id.dynamic_layout)

        // Show the dynamic views layout visible
        dynamicLayout.visibility = View.VISIBLE
        if (jobInfo.listOfAttributes != null) {
            AttributeSectionFragment.addAttributeSections(this@JobDetailsActivity,
                    dynamicLayout, jobInfo.listOfAttributes!!, false,
                    false, true, false, jobInfo.additionalRequirements)
        }

    }

    class JobDetailAvailabilityFragment : AvailabilityFragmentNew() {
        override fun getParentView(inflater: LayoutInflater?, container: ViewGroup?): View {
            return inflater!!.inflate(R.layout.availability_fragment_new, container, false)
        }
    }
}


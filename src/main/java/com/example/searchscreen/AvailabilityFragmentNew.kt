package com.example.searchscreen

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.TypedValue
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TableRow
import android.widget.TextView
import com.example.searchscreen.R
import com.care.patternlib.CareCheckBox


/**
 * Constructor
 */
open class AvailabilityFragmentNew : Fragment() {

    /**
     * View member variable
     */
    private var mParentView: View? = null

    /**
     * Data member variable
     */
    private var mResult = Array(7) { BooleanArray(7) }
    private var mMode = Mode.EDIT_MODE
    private var mRecreated: Boolean = false

    enum class Mode
    (
        val value: Int) {
        VIEW_MODE(0),
        EDIT_MODE(1),
        COLLECT_MODE(2);


        companion object {
            // Get mode
            fun getMode(value: Int): Mode {
                val modes = arrayOf(VIEW_MODE, EDIT_MODE, COLLECT_MODE)
                return modes[value]
            }
        }
    }

    /**
     * On create
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true

        if (savedInstanceState != null) {
            for (i in 0..6) {
                mResult[i] = savedInstanceState.getBooleanArray("result_$i")
            }
            mMode = Mode.getMode(savedInstanceState.getInt("mode"))
            mRecreated = true
        } else {
            mRecreated = false
        }
    }

    /**
     * on create view
     */
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        mParentView = getParentView(inflater, container)
        return mParentView
    }

    /**
     * save the values to the bundle
     */
    override fun onSaveInstanceState(outState: Bundle?) {
        for (i in 0..6) {
            outState!!.putBooleanArray("result_$i", mResult[i])
        }
        outState!!.putInt("mode", mMode.value)

        super.onSaveInstanceState(outState)
    }

    /**
     * Get parent view
     */
    open fun getParentView(inflater: LayoutInflater?, container: ViewGroup?): View {
        val view = inflater!!.inflate(com.care.patternlib.R.layout.job_activity_availabilityl_fragment, container, false)
        mMode = Mode.EDIT_MODE

        for (i in 0..6) {
            val layout = view.findViewById(
                    activity.resources.getIdentifier("layout_" + (i + 1), "id", activity.packageName)) as TableRow

            val sundayChk = layout.findViewById(R.id.sunday_text) as CareCheckBox
            val sundayLayout = layout.findViewById(R.id.container_layout_1) as LinearLayout
            sundayChk.setDrawable(R.drawable.checkbox_calendar_pattern)
            setClickListener(sundayChk, sundayLayout, i, 0)
            sundayChk.state = mResult[0][i]

            val mondayChk = layout.findViewById(R.id.monday_text) as CareCheckBox
            val mondayLayout = layout.findViewById(R.id.container_layout_2) as LinearLayout
            mondayChk.setDrawable(R.drawable.checkbox_calendar_pattern)
            setClickListener(mondayChk, mondayLayout, i, 1)
            mondayChk.state = mResult[1][i]

            val tuesdayChk = layout.findViewById(R.id.tuesday_text) as CareCheckBox
            val tuesdayLayout = layout.findViewById(R.id.container_layout_3) as LinearLayout
            tuesdayChk.setDrawable(R.drawable.checkbox_calendar_pattern)
            setClickListener(tuesdayChk, tuesdayLayout, i, 2)
            tuesdayChk.state = mResult[2][i]

            val wednesdayChk = layout.findViewById(R.id.wednesday_text) as CareCheckBox
            val wednesdayLayout = layout.findViewById(R.id.container_layout_4) as LinearLayout
            wednesdayChk.setDrawable(R.drawable.checkbox_calendar_pattern)
            setClickListener(wednesdayChk, wednesdayLayout, i, 3)
            wednesdayChk.state = mResult[3][i]

            val thursdayChk = layout.findViewById(R.id.thursday_text) as CareCheckBox
            val thursdayLayout = layout.findViewById(R.id.container_layout_5) as LinearLayout
            thursdayChk.setDrawable(R.drawable.checkbox_calendar_pattern)
            setClickListener(thursdayChk, thursdayLayout, i, 4)
            thursdayChk.state = mResult[4][i]

            val fridayChk = layout.findViewById(R.id.friday_text) as CareCheckBox
            val fridayLayout = layout.findViewById(R.id.container_layout_6) as LinearLayout
            fridayChk.setDrawable(R.drawable.checkbox_calendar_pattern)
            setClickListener(fridayChk, fridayLayout, i, 5)
            fridayChk.state = mResult[5][i]

            val saturdayChk = layout.findViewById(R.id.saturday_text) as CareCheckBox
            val saturdayLayout = layout.findViewById(R.id.container_layout_7) as LinearLayout
            saturdayChk.setDrawable(R.drawable.checkbox_calendar_pattern)
            setClickListener(saturdayChk, saturdayLayout, i, 6)
            saturdayChk.state = mResult[6][i]
        }
        return view
    }

    /**
     * Update availability fields with job details
     */
    fun updateViews(jobInfo: JobDetails, mode: Mode) {
        mMode = mode

        if (jobInfo.availability == null) {
            return
        }
        mResult = jobInfo.availability!!
        updateAvailability(mMode, "", null)
    }


    /**
     * set the title for availability fragment
     */
    fun setTitle(title: String?) {
        val titleView = activity.findViewById(R.id.title) as TextView

        if (title != null) {
            titleView.visibility = View.VISIBLE
            titleView.text = title
        }
    }

    /**
     * show or hide title
     */
    fun showTitle(show: Boolean) {
        val titleView = activity.findViewById(R.id.title) as TextView
        val visibility = if (show) View.VISIBLE else View.GONE

        titleView.visibility = visibility
    }

    /**
     * Helper method to set click listener
     */
    private fun setClickListener(checkedTextView: CareCheckBox, layout: LinearLayout,
                                 col: Int, cnt: Int) {
        layout.setOnClickListener {
            checkedTextView.state = !checkedTextView.isSelected
            mResult[cnt][col] = checkedTextView.isSelected
            checkedTextView.invalidate()
        }
    }

    /**
     * update availability with content
     */
    private fun updateAvailability(mode: Mode, key: String, updatedAttributes: Any?) {
        for (i in 0..6) {
            var layout: TableRow = mParentView!!.findViewById(R.id.layout_1) as TableRow
            when (i) {
                0 -> layout = mParentView!!.findViewById(R.id.layout_1) as TableRow
                1 -> layout = mParentView!!.findViewById(R.id.layout_2) as TableRow
                2 -> layout = mParentView!!.findViewById(R.id.layout_3) as TableRow
                3 -> layout = mParentView!!.findViewById(R.id.layout_4) as TableRow
                4 -> layout = mParentView!!.findViewById(R.id.layout_5) as TableRow
                5 -> layout = mParentView!!.findViewById(R.id.layout_6) as TableRow
                6 -> layout = mParentView!!.findViewById(R.id.layout_7) as TableRow
            }
            val sundayChk = layout.findViewById(R.id.sunday_text) as CareCheckBox
            sundayChk.setDrawable(R.drawable.checkbox_calendar_pattern)
            sundayChk.setPadding(dp2px(6f), dp2px(10f), dp2px(6f), dp2px(10f))
            val sundayLayout = layout.findViewById(R.id.container_layout_1) as LinearLayout
            updateAvailabilityField(mode, 0, i, sundayLayout, sundayChk)

            val mondayChk = layout.findViewById(R.id.monday_text) as CareCheckBox
            val mondayLayout = layout.findViewById(R.id.container_layout_2) as LinearLayout
            mondayChk.setDrawable(R.drawable.checkbox_calendar_pattern)
            updateAvailabilityField(mode, 1, i, mondayLayout, mondayChk)

            val tuesdayChk = layout.findViewById(R.id.tuesday_text) as CareCheckBox
            val tuesdayLayout = layout.findViewById(R.id.container_layout_3) as LinearLayout
            tuesdayChk.setDrawable(R.drawable.checkbox_calendar_pattern)
            updateAvailabilityField(mode, 2, i, tuesdayLayout, tuesdayChk)

            val wednesdayChk = layout.findViewById(R.id.wednesday_text) as CareCheckBox
            val wednesdayLayout = layout.findViewById(R.id.container_layout_4) as LinearLayout
            wednesdayChk.setDrawable(R.drawable.checkbox_calendar_pattern)
            updateAvailabilityField(mode, 3, i, wednesdayLayout, wednesdayChk)

            val thursdayChk = layout.findViewById(R.id.thursday_text) as CareCheckBox
            val thursdayLayout = layout.findViewById(R.id.container_layout_5) as LinearLayout
            thursdayChk.setDrawable(R.drawable.checkbox_calendar_pattern)
            updateAvailabilityField(mode, 4, i, thursdayLayout, thursdayChk)

            val fridayChk = layout.findViewById(R.id.friday_text) as CareCheckBox
            val fridayLayout = layout.findViewById(R.id.container_layout_6) as LinearLayout
            fridayChk.setDrawable(R.drawable.checkbox_calendar_pattern)
            updateAvailabilityField(mode, 5, i, fridayLayout, fridayChk)

            val saturdayChk = layout.findViewById(R.id.saturday_text) as CareCheckBox
            val saturdayLayout = layout.findViewById(R.id.container_layout_7) as LinearLayout
            saturdayChk.setDrawable(R.drawable.checkbox_calendar_pattern)
            updateAvailabilityField(mode, 6, i, saturdayLayout, saturdayChk)
        }
    }

    /**
     * update availability field
     */
    private fun updateAvailabilityField(mode: Mode, row: Int, col: Int, layout: LinearLayout,
                                        checkedTextView: CareCheckBox) {
        // view/edit mode
        if (mode == Mode.VIEW_MODE || mode == Mode.EDIT_MODE) {
            checkedTextView.state = mResult[row][col]
            checkedTextView.isEnabled = mode == Mode.EDIT_MODE
            layout.isClickable = mode == Mode.EDIT_MODE
        } else if (mode == Mode.COLLECT_MODE) {
            mResult[row][col] = checkedTextView.isSelected
        }// collect mode
    }

    /**
     * To convert dp to px
     * @param dp  value
     * @return px value
     */
    private fun dp2px(dp: Float): Int {
        val r = activity.resources
        val px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.displayMetrics)
        return Math.round(px)
    }
}